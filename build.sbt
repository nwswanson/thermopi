name := "thermopi"

version := "0.1"

scalaVersion := "2.11.8"

resolvers += "sparetimelabs" at "http://www.sparetimelabs.com/maven2/"
resolvers += "jitpack" at "https://jitpack.io"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % "2.4.1",
  "com.typesafe.play" %% "play" % "2.5.1",
  "io.reactivex" %% "rxscala" % "0.26.0",
//  "net.liftweb" %% "lift-json" % "2.6",
  "org.scala-lang.modules" %% "scala-async" % "0.9.5",
  "com.sparetimelabs" % "purejavacomm" % "1.0.0"
)

mainClass in assembly := Some("com.nathanielswanson.thermopi.Main")
mainClass in (Compile, run) := Some("com.nathanielswanson.thermopi.Main")
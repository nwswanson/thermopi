package com.nathanielswanson.thermopi
import akka.actor.Props
import scala.concurrent.duration._
import akka.actor.{ActorSystem, Cancellable}


object DataTypes {
  type PotentialArgs = Option[List[String]]
  type FlagMap = Map[String, PotentialArgs]
}

object Main {

  var preferenceFlags : DataTypes.FlagMap = Map.empty
  var arguments: DataTypes.PotentialArgs = None

  // TODO: This is eventually going to be the tick for the local system to schedule
  // sensor polls.
  val system = ActorSystem("system")

  import system.dispatcher

  def main(args: Array[String]) {
    def splitPrefix(list: List[String]) : (DataTypes.PotentialArgs, DataTypes.FlagMap) = {
      var values : DataTypes.FlagMap  = Map.empty

      def argsAndTail(args:List[String]): (DataTypes.PotentialArgs, List[String]) = {
        val (additions: List[String], trueTail) = args span (!_.startsWith("--"))
        val payload: DataTypes.PotentialArgs = if (additions.isEmpty) None else Some(additions)
        (payload, trueTail)
      }

      def split(args: List[String]): List[String] = {
        args match {
          case Nil => List()
          case head::tail =>
            val (payload, trueTail) = argsAndTail(tail)
            values = values ++ Map(head -> payload)
            split(trueTail)
        }
      }
      val (arguments, flagTail) = argsAndTail(list)
      split(flagTail)
      (arguments, values)
    }

    val parsed = splitPrefix(args.toList)
    arguments = parsed._1
    preferenceFlags = parsed._2
    preferenceFlags.getOrElse("--mode", None) match {
      case Some(List("backend")) =>
        println("Running as a processing backend.")
        startAsBackend()
      case Some(List("reporter")) =>
        println("Running as reporter.")
        startAsReporter()
      // This is the default.
      case Some(List("standalone")) | _ =>
        println("Running in standalone mode.")
        startAsStandalone()
    }
  }
  def startAsStandalone() = {
    val sensorActorReference = Props(classOf[SensorActor], preferenceFlags, Map())
    val genericSensor = system.actorOf(sensorActorReference, name = "SensorActor")
    val systemsScheduler = system.actorOf(Props[SystemsScheduler], name = "SystemsScheduler")
    val cancel = system.scheduler.schedule(
      0.milliseconds,
      1.seconds,
      genericSensor,
      Echo("ping")
    )
    genericSensor ! DebugLogState()
  }
  def startAsBackend(){}
  def startAsReporter(){}
}

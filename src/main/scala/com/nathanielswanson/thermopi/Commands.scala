package com.nathanielswanson.thermopi
import scala.concurrent.duration._

/**
  * Created by nate on 4/10/16.
  */

abstract class Command

case class Echo(message:String) extends Command
case class Shutdown() extends Command
case class ShutdownAfter(time:FiniteDuration) extends Command
case class Add(item:Any) extends Command
case class Remove(item:Any) extends Command
case class Poll() extends Command
case class Start() extends Command
case class ChangeSettings(settings: Any) extends Command
case class UpdateItem(identifier:Object, value: Any) extends Command
case class Stop() extends Command
case class DebugLogState() extends Command


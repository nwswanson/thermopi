package com.nathanielswanson.thermopi

import akka.actor.Actor

/**
  * Created by Genevieve on 4/9/16.
  */
class SensorActor(systemSettings: DataTypes.FlagMap,
                  actorSettings: Map[String, String]) extends Actor with akka.actor.ActorLogging {
  def receive = {
    case DebugLogState() =>
      log.info("System settings are: " + systemSettings.toString)
      log.info("Actor settings are: " + actorSettings.toString)
    case Echo(s) =>
      log.info(s)
    case _ =>
      log.warning("Unknown message received.")
  }
}


package com.nathanielswanson.thermopi

import akka.actor.Actor
import akka.actor.ActorRef
import scala.concurrent.duration._

/**
  * Created by nate on 4/9/16.
  */

class SystemsScheduler extends Actor with akka.actor.ActorLogging {
  import scala.concurrent.ExecutionContext.Implicits.global

  var actors : List[ActorRef] = List.empty

  def receive = {
    case Echo(s) =>
      log.info(s)
    case Shutdown() =>
      context.system.terminate()
    case ShutdownAfter(duration) =>
      context.system.scheduler.scheduleOnce(duration){
        context.system.terminate()
      }
    case _ => Nil
  }
}
